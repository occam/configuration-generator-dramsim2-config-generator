import os
import pdb
import json
from collections import OrderedDict

from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
job_path       = os.getcwd()

object = Occam.load()


################################################################################
#                                                                              #
#                               Get output path                                #
#                                                                              #
################################################################################
# Create a new object
output_path  = object.path()
configuration_path = os.path.join(output_path, "new_configuration")

#Unless there is an output
outputs = object.outputs("configuration")
if len(outputs) > 0:
    configuration = outputs[0]
    configuration_path = configuration.volume()
else:
    os.mkdir(configuration_path)

if not os.path.exists(configuration_path):
    os.mkdir(configuration_path)

print("Configuration path: %s"%(configuration_path))



################################################################################
#                                                                              #
#                       Generate system configuration                          #
#                                                                              #
################################################################################

# Open object.json for command line options
sys_data = object.configuration("System Configuration")

# Generate input
sysfile=os.path.join(configuration_path,'system.ini')
print( "Generating file: %s" %(sysfile))
input_file = open(sysfile, 'w+')

for k, v in sys_data.items():
    if isinstance(v, dict):
        for k,v in v.items():
            input_file.write(k.upper()+"="+str(v)+"\n")
    else:
        input_file.write(k.upper()+"="+str(v)+"\n")

################################################################################
#                                                                              #
#                      Generate DRAM configuration                             #
#                                                                              #
################################################################################
dram_data = object.configuration("DRAM Configuration")

# Generate input
devfile=os.path.join(configuration_path,'device.ini')
print( "Generating file: %s" %(devfile))
input_file = open(devfile, 'w+')

for k, v in dram_data.items():
    if isinstance(v, dict):
        for k,v in v.items():
            input_file.write(k+"="+str(v)+"\n")
    else:
        input_file.write(k+"="+str(v)+"\n")

input_file.close()

################################################################################
#                                                                              #
#                       Process general configuration                          #
#                                                                              #
################################################################################
general_configuration = object.configuration("General Options")
if(os.path.exists(os.path.join(configuration_path, 'object.json'))):
    f = open(os.path.join(configuration_path, 'object.json'), 'r')
    output_object = OrderedDict(json.load(f))
    f.close()
else:
    output_object = OrderedDict({})
print(json.dumps(output_object, indent=4, separators=(',', ': ')))

output_object["name"] = general_configuration["name"]
output_object["type"] = "Configuration"
output_object["subtype"] = "DRAMSIM2"
print("writting object to: %s"%(os.path.join(configuration_path, 'object.json')))
f = open(os.path.join(configuration_path, 'object.json'), 'w+')
f.write(json.dumps(output_object, indent=4, separators=(',', ': ')))
f.close()

if(os.path.exists(os.path.join(configuration_path, 'object.json'))):
    f = open(os.path.join(configuration_path, 'object.json'), 'r')
    output_object = OrderedDict(json.load(f))
    f.close()
else:
    output_object = OrderedDict({})
print(json.dumps(output_object, indent=4, separators=(',', ': ')))


# Done
warnings = []
errors = []
Occam.finish(warnings, errors)

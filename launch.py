import os
import pdb
import json
from collections import OrderedDict
from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
job_path       = os.getcwd()

# Done
execute = os.path.join(scripts_path, 'execute.py')
args = [
    'python',
    execute
]
command = ' '.join(args)
Occam.report(command)

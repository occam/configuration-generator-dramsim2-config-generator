{
    "NUM_BANKS": {
        "type": "int",
        "default": 8,
        "label": "Number of Banks",
        "description": "The number of device banks."
    },

    "NUM_ROWS": {
        "type": "int",
        "default": 32768,
        "label": "Number of Rows",
        "description": "The number of device rows."
    },

    "NUM_COLS": {
        "type": "int",
        "default": 2048,
        "label": "Number of Columns",
        "description": "The number of device columns."
    },

    "DEVICE_WIDTH": {
        "type": "int",
        "default": 4,
        "label": "Device Width",
        "description": "The device width."
    },

    "REFRESH_PERIOD": {
        "type": "int",
        "default": 7800,
        "label": "Refresh Period",
        "description": "Time period to refresh all DRAM cells."
    },

    "tCK": {
        "type": "float",
        "default": 1.5,
        "label": "Clock Cycle Time",
        "description": " The length of a single memory clock tick."
    },

    "CL": {
        "type": "int",
        "default": 10,
        "label": "CAS Latency (Column Address Strobe Time)",
        "description": "Number of clocks that elapses between the memory controller telling the memory module to access a particular column in the current row, and the data from that column being read from the module's output pins."
    },

    "AL": {
        "type": "int",
        "default": 0,
        "label": "Additive Latency",
        "description": "The time period that the command is held before it is issued inside the DRAM device. "
    },

    "BL": {
        "type": "int",
        "default": 8,
        "label": "Burst Length",
        "description": "The time to transer the data that is read after a read command or written after a write command."
    },

    "tRAS": {
        "type": "int",
        "default": 24,
        "label": "Active to Precharge Delay (Row Address Strobe Time)",
        "description": "Number of clocks taken between a bank active command and issuing the precharge command."
    },

    "tRCD": {
        "type": "int",
        "default": 10,
        "label": "RAS to CAS Delay(Row to Column Command Delay) ",
        "description": "Number of clocks inserted between a row activate command and a read or write command to that row."
    },

    "tRRD": {
        "type": "int",
        "default": 4,
        "label": "Row Activation to Row Activation Delay",
        "description": "Number of clocks between two row activate in different banks of the same rank."
    },

    "tRC": {
        "type": "int",
        "default": 34,
        "label": "Random Read or Write Cycle Time ",
        "description": "Minimum number of clock cycles a memory row takes to complete a full cycle, from row activation up to the precharging of the active row."
    },

    "tRP": {
        "type": "int",
        "default": 10,
        "label": "RAS Precharge",
        "description": "Controls the number of clocks that are inserted between a row precharge command and an activate command to the same rank."
    },

    "tCCD": {
        "type": "int",
        "default": 4,
        "label": "Column to Column Delay",
        "description": "The minimum CAS to CAS."
    },

    "tRTP": {
        "type": "int",
        "default": 5,
        "label": "Read to Precharge Delay",
        "description": "Number of clocks that are inserted between a read command to a row pre-charge command to the same rank."
    },

    "tWTR": {
        "type": "int",
        "default": 5,
        "label": "Write to Read Delay",
        "description": "Number of clock between the last valid write operation and the next read command to the same internal bank."
    },

    "tWR": {
        "type": "int",
        "default": 10,
        "label": "Write Recovery Time",
        "description": "Amount of clock cycles that must elapse after the completion of a valid write operation before an active bank can be precharged."
    },

    "tRTRS": {
        "type": "int",
        "default": 1,
        "label": "Rank to Rank Switching Time",
        "description": "The amount of time that elapses between switching from one rank to another."
    },

    "tRFC": {
        "type": "int",
        "default": 107,
        "label": "Row Refresh Cycle Time",
        "description": "Number of clock measured from a refresh command (REF) until the first activate command (ACT) to the same rank."
    },

    "tFAW": {
        "type": "int",
        "default": 20,
        "label": "Four (Row) Bank Activation Window",
        "description": "Time interval in which four activates are allowed the same rank."
    },

    "tCKE": {
        "type": "int",
        "default": 4,
        "label": "Clock Enable",
        "description": "Must be kept 'Low' to maintain the memory in the Self-Refresh mode. When exiting the Self-Refresh mode, it must be ensured that the clock is stable and then, the CKE can be changed to 'High'."
    },

    "tXP": {
        "type": "int",
        "default": 4,
        "label": "Low Power Mode Timing Constraint",
        "description": "Timing contraint for low power states."
    },

    "tCMD": {
        "type": "int",
        "default": 1,
        "label": "Command Transport Duration",
        "description": "The time period that a command occupies on the cmd bus as it is transported from the Memory Controller to the DRAM devices."
    },

    "DRAM Device Power Consumption Parameters": {
        "IDD0": {
            "type": "int",
            "default": 100,
            "label": "Operating One Bank Active-Precharge Current (IDD0)",
            "description": "tCK = tCK(IDD), tRC = tRC(IDD), tRAS = tRASmin(IDD). CKE is HIGH, CS# is HIGH between valid commands. Address bus inputs are SWITCHING. Data bus inputs are SWITCHING."
        },

        "IDD1": {
            "type": "int",
            "default": 130,
            "label": "Operating One Bank Active-Read-Precharge Current (IDD1)",
            "description": "IOUT = 0mA; BL = 4, CL = CL(IDD), AL = 0; tCK = tCK(IDD), tRC =tRC(IDD), tRAS = tRASmin(IDD), tRCD = tRCD(IDD). CKE is HIGH, CS# is HIGH between valid commands. Address bus inputs are SWITCHING. Data bus inputs are SWITCHING."
        },

        "IDD2P": {
            "type": "int",
            "default": 10,
            "label": "Precharge Power-Down Current (IDD2P)",
            "description": "All banks idle. tCK = tCK(IDD). CKE is LOW. Other control and address bus inputs are STABLE. Data bus inputs are FLOATING."
        },

        "IDD2Q": {
            "type": "int",
            "default": 70,
            "label": "Precharge Quiet Standby Current (IDD2Q)",
            "description": "All banks idle. tCK = tCK(IDD). CKE is HIGH, CS# is HIGH. Other control and address bus inputs are STABLE. Data bus inputs are FLOATING."
        },

        "IDD2N": {
            "type": "int",
            "default": 70,
            "label": "Precharge Standby Current (IDD2N)",
            "description": "All banks idle. tCK = tCK(IDD). CKE is HIGH, CS# is HIGH. Other control and address bus inputs are SWITCHING. Data bus inputs are SWITCHING."
        },

        "IDD3Pf": {
            "type": "int",
            "default": 60,
            "label": "Active Power-Down Current (IDD3PF)",
            "description": "All banks open. tCK = tCK(IDD). CKE is LOW. Other control and address bus inputs are STABLE. Data bus inputs are FLOATING."
        },

        "IDD3Ps": {
            "type": "int",
            "default": 60,
            "label": "Active Power-Down Current (IDD3PS)",
            "description": "All banks open. tCK = tCK(IDD). CKE is LOW. Other control and address bus inputs are STABLE. Data bus inputs are FLOATING."
        },

        "IDD3N": {
            "type": "int",
            "default": 90,
            "label": "Active Standby Current (IDD3N)",
            "description": "All banks open. tCK = tCK(IDD), tRAS = tRASmax(IDD), tRP = tRP(IDD). CKE is HIGH, CS# is HIGH between valid commands. Other control and address bus inputs are SWITCHING. Data bus inputs are SWITCHING."
        },

        "IDD4W": {
            "type": "int",
            "default": 255,
            "label": "Operating Burst Write Current (IDD4W)",
            "description": "All banks open, continuous burst writes. BL = 4, CL = CL(IDD), AL = 0. tCK = tCK(IDD), tRAS = tRASmax(IDD), tRP = tRP(IDD). CKE is HIGH, CS# is HIGH between valid commands. Address bus inputs are SWITCHING. Data bus inputs are SWITCHING."
        },

        "IDD4R": {
            "type": "int",
            "default": 230,
            "label": "Operating Burst Read Current (IDD4R)",
            "description": "All banks open, Continuous burst reads, IOUT = 0mA. BL = 4, CL = CL(IDD), AL = 0. tCK = tCK(IDD), tRAS = tRASmax(IDD), tRP = tRP(IDD). CKE is HIGH, CS# is HIGH between valid commands. Address bus inputs are SWITCHING. Data bus inputs are SWITCHING."
        },

        "IDD5": {
            "type": "int",
            "default": 295,
            "label": "Burst Auto Refresh Current (IDD5)",
            "description": "tCK = tCK(IDD). Refresh command at every tRFC(IDD) interval. CKE is HIGH, CS# is HIGH between valid commands. Other control and address bus inputs are SWITCHING. Data bus inputs are SWITCHING."
        },

        "IDD6": {
            "type": "int",
            "default": 9,
            "label": "Self Refresh Current (IDD6)",
            "description": "CK and CK# at 0V. CKE = 0.2V. Other control and address bus inputs are FLOATING. Data bus inputs are FLOATING."
        },

        "IDD6L": {
            "type": "int",
            "default": 12,
            "label": "Self Refresh Current (IDD6L)",
            "description": "CK and CK# at 0V. CKE = 0.2V. Other control and address bus inputs are FLOATING. Data bus inputs are FLOATING."
        },

        "IDD7": {
            "type": "int",
            "default": 299,
            "label": "Operating Bank Interleave Read Current (IDD7)",
            "description": "All bank interleaving reads, IOUT = 0mA. BL = 4, CL = CL(IDD), AL = tRCD(IDD)-1*tCK(IDD). tCK = tCK(IDD), tRC = tRC(IDD), tRRD = tRRD(IDD), tRCD = 1*tCK(IDD). CKE is HIGH, CS# is HIGH between valid commands. Address bus inputs are STABLE during deselects. Data bus inputs are SWITCHING."
        },

        "Vdd": {
            "type": "float",
            "default": 1.5,
            "label": "Voltage",
            "description": "Internal supply voltage for the memory core"
        }
    }
}
